// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "OverloadBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UOverloadBuff : public UBuffComponent
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	virtual void BuffEffect() override;
	virtual void RevertBuff() override;
	
};
