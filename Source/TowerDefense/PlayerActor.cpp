// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerActor.h"
#include "PCore.h"
#include "HealthComponent.h"
#include "EnemyAI.h"

// Sets default values
APlayerActor::APlayerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
}

// Called when the game starts or when spawned
void APlayerActor::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < Cores.Num(); i++)
	{
		Cores[i]->OnTrigger.AddDynamic(this, &APlayerActor::OnHit);
	}
}

// Called every frame
void APlayerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerActor::OnHit(AEnemyAI* Enemy)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Enemy Hits Core"));
	Health->TakeDamage(Enemy->Damage);
}

