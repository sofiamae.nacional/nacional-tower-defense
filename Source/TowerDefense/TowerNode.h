// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerNode.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerNode();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USceneComponent* SceneComponent;
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool canPlaceTower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* m_occupied;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* m_vacant;

	UFUNCTION(BlueprintCallable)
		void SpawnOnNode(TSubclassOf<class ATower> Tower);
};
