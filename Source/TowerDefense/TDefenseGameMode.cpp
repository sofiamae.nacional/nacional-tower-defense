// Fill out your copyright notice in the Description page of Project Settings.


#include "TDefenseGameMode.h"
#include "EnemyAI.h"
#include "SpawnPoint.h"
#include "WaveData.h"
#include "HealthComponent.h"
#include "BuildManager.h"
#include "PlayerActor.h"
#include "PCore.h"
#include "TDPlayerController.h"
#include "Engine/EngineTypes.h"
#include "TimerManager.h"

void ATDefenseGameMode::BeginPlay()
{
	Super::BeginPlay();
}

void ATDefenseGameMode::StartGame()
{
	for (int i = 0; i < Player->Cores.Num(); i++)
	{
		Player->Cores[i]->OnTrigger.AddDynamic(this, &ATDefenseGameMode::EnemyOnHit);
	}

	StartSpawn();
	curWave = 0;
	curSpawn = 0;
}

void ATDefenseGameMode::StartSpawn()
{
	GetWorldTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &ATDefenseGameMode::SpawnInterval), 0.5f, false);
}

void ATDefenseGameMode::SpawnInterval()
{
	if (curWave <= Waves.Num())
	{
		if (curSpawn < Waves[curWave]->NumSpawns)
		{
			AEnemyAI* EnemyRef = Spawner[FMath::RandRange(0, 3)]->spawnEnemies(Waves[curWave]->Enemies);
			EnemyRef->Health->OnDeath.AddDynamic(this, &ATDefenseGameMode::AddGold); // kill reward
			EnemyRef->Health->OnDeathNoParam.AddDynamic(this, &ATDefenseGameMode::WaveProgression);
			EnemyRef->Health->maxHp = Waves[curWave]->EnemyHealth;
			EnemyRef->Health->curHp = EnemyRef->Health->maxHp;
			curSpawn++;
		}
		else
		{
			//WaveCleared();
		}

		GetWorldTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &ATDefenseGameMode::SpawnInterval), 5.0f, false);
	}
	else if (curWave == Waves.Num())
	{
		WavesCompleted();
	}
}

void ATDefenseGameMode::AddGold(UHealthComponent* Health)
{
	if (AEnemyAI* Enemy = Cast<AEnemyAI>(Health->GetOwner()))
	{
		Player->Gold += Waves[curWave]->KillReward;
	}
}

void ATDefenseGameMode::WaveProgression()
{
	killCount++;
	progressPercent = killCount / Waves[curWave]->NumSpawns; // getter

	if (killCount >= Waves[curWave]->NumSpawns)
	{
		WaveCleared();
	}
}

void ATDefenseGameMode::EnemyOnHit(AEnemyAI* Enemy)
{
	WaveProgression();
}

void ATDefenseGameMode::WaveCleared()
{
	Player->Gold += Waves[curWave]->ClearReward;
	curSpawn = 0;
	killCount = 0;
	progressPercent = 0;
	curWave++;
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Wave cleared"));
}

void ATDefenseGameMode::WavesCompleted()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("All Waves COMPLETE"));
}

