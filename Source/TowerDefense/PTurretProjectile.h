// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "PTurretProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API APTurretProjectile : public AProjectile
{
	GENERATED_BODY()

protected:
	virtual void Tick(float DeltaTime) override;
public:
	virtual void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult) override;

};
