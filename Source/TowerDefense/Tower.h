// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

UCLASS()
class TOWERDEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USceneComponent* SceneComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* GunTurret;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* Range;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* Arrow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrading")
		int32 MaxUpgrade;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrading")
		bool canBuy;

	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// General Components
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		class UTowerData* TowerData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		TArray<class AActor*> UnitsInRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		bool isSpawned; // for ghost
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General") // where built
		class ATowerNode* Node;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		class UMaterial* CurrentMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		float SphereRadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		float AoERadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		int32 PassiveGold;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		int32 Cost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		int32 UpgradeCost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		int32 SellCost;


	UFUNCTION(BlueprintNativeEvent)
		void OnBeginOverlap(class UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);
	UFUNCTION(BlueprintCallable)
		void OnEndOverlap(class UPrimitiveComponent* OverlappedComp,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);
	UFUNCTION(BlueprintCosmetic)
		void Initialize();
	UFUNCTION(BlueprintCallable)
		void UI_Initialize();
	UFUNCTION(BlueprintImplementableEvent)
		void UI_Initialize_Evt();
	UFUNCTION()
		void AddUnitsInRange();

	UFUNCTION(BlueprintImplementableEvent) // Shooter towers' delay
		void TimerDelay();
	
	// For Shooting Towers
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooter Tower")
		bool isShooter; // true if Turret, Missile, Laser Tower
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooter Tower")
		bool targetsEnemy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooter Tower")
		bool canShoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooter Tower")
		float fireRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooter Tower")
		int32 Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooter Tower")
		TSubclassOf<class AProjectile> Projectile;
	
	UFUNCTION()
		virtual void LookAtEnemy();
	UFUNCTION(BlueprintImplementableEvent)
		void SpawnProjectile();
	UFUNCTION()
		void Shoot();

	// For Buff Towers
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Tower")
		TSubclassOf<class UBuffComponent> Buff;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff Tower")
		float BuffStrength;

	UFUNCTION(BlueprintImplementableEvent)
		void AttachBuff(class AActor* target);
	UFUNCTION(BlueprintImplementableEvent)
		void RevertEffect(class AActor* target);

	// For Upgrading
	UFUNCTION(BlueprintCallable)
		void UpgradeTower(APlayerActor* Player);
	UFUNCTION(BlueprintImplementableEvent)
		void UpgradeTower_Evt();

	UFUNCTION(BlueprintCallable)
		void ReapplyBuff(AActor* Actor);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrading")
		int32 UpgradeCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrading")
		bool canUpgrade;
};
