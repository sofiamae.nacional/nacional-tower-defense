// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"

UCLASS()
class TOWERDEFENSE_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	APlayerController* PlayerController;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerActor* Player;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool canBuy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class UTowerData*> TowerData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATowerGhost* TowerGhost;

	UFUNCTION(BlueprintCallable)
		void SpawnGhost(TSubclassOf<class ATowerGhost> Ghost);
	UFUNCTION(BlueprintCallable)
		bool checkGold(int32 TowerCost);
	UFUNCTION(BlueprintCallable)
		void BuyTower(int32 TowerCost);
	UFUNCTION(BlueprintCallable)
		void BuildTower(ATowerNode* Node, TSubclassOf<class ATower> Tower);

	UFUNCTION(BlueprintCallable)
		void SellTower(class ATower* TowerToSell);
};
