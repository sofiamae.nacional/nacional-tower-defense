// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildManager.h"
#include "PlayerActor.h"
#include "Tower.h"
#include "TowerNode.h"
#include "TowerGhost.h"
#include "TowerData.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABuildManager::ABuildManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuildManager::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	
}

// Called every frame
void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (TowerGhost)
	{
		FHitResult Hit;
		PlayerController->GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, Hit);

		if (ATowerNode* Node = Cast<ATowerNode>(Hit.GetActor()))
		{
			TowerGhost->SetActorLocationAndRotation(Node->GetActorLocation(), Node->GetActorRotation());
		}
		else
		{
			TowerGhost->SetActorLocation(Hit.Location);
		}
	}

}

void ABuildManager::SpawnGhost(TSubclassOf<class ATowerGhost> Ghost)
{
	FHitResult Hit;
	FActorSpawnParameters SpawnParam;
	
	TowerGhost = GetWorld()->SpawnActor<ATowerGhost>(Ghost, Hit.Location, FRotator(0), SpawnParam);
	//TowerGhost->isSpawned;

}

bool ABuildManager::checkGold(int32 TowerCost)
{
	if (Player->Gold >= TowerCost)
	{
		canBuy = true;
	}
		
	else
		canBuy = false;

	return canBuy;
}

void ABuildManager::BuyTower(int32 TowerCost)
{
	if (checkGold(TowerCost))
	{
		Player->Gold -= TowerCost;
	}
	else
	{
		TowerGhost->Destroy();
	}
	
}

void ABuildManager::BuildTower(ATowerNode* Node, TSubclassOf<class ATower> Tower)
{
	TowerGhost->Destroy();
	FActorSpawnParameters SpawnParam;
	ATower* TowerRef = GetWorld()->SpawnActor<ATower>(Tower, Node->StaticMesh->GetComponentLocation(), Node->StaticMesh->GetComponentRotation(), SpawnParam);
	Node->canPlaceTower = false;
	TowerRef->Node = Node;
	TowerRef->isSpawned = true;
	TowerRef->Initialize();
	TowerRef->AddUnitsInRange();
}

void ABuildManager::SellTower(ATower* TowerToSell)
{
	if (TowerToSell)
	{
		Player->Gold += TowerToSell->SellCost;

		if (ATowerNode* Node = Cast<ATowerNode>(TowerToSell->Node))
		{
			Node->canPlaceTower = true;
		}
		TowerToSell->RevertEffect(TowerToSell);
		TowerToSell->Destroy();
	}
	
}

