// Fill out your copyright notice in the Description page of Project Settings.


#include "PMissileProjectile.h"
#include "EnemyAI.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/SphereComponent.h"

void APMissileProjectile::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnBeginOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	
	if (AEnemyAI* Enemy = Cast<AEnemyAI>(OtherActor))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("missile hit"));
		FVector SphereSpawnLoc = Enemy->GetActorLocation();
		TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;

		TraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic));
		TArray<AActor*> Enemies;
		TArray<AActor*> IgnoredActors;

		UClass* seekClass = AEnemyAI::StaticClass();
		UKismetSystemLibrary::SphereOverlapActors(GetWorld(), SphereSpawnLoc, AoeRange, TraceObjectTypes, seekClass, IgnoredActors, Enemies);
		UKismetSystemLibrary::DrawDebugSphere(GetWorld(), Enemy->GetActorLocation(), AoeRange, 12, FColor::Red, true, 15.0f);
		//UE_LOG(LogTemp, Warning TEXT((Text, ), Enemies));
		
		for (auto actor : Enemies)
		{
			if (AEnemyAI* enemy = Cast<AEnemyAI>(actor))
			{
				ApplyDamage(enemy);
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("missile damage"));
			}
		}
		Destroy();
	}
}

