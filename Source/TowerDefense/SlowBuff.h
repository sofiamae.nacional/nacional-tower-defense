// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "SlowBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API USlowBuff : public UBuffComponent
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	virtual void BuffEffect() override;
	virtual void RevertBuff() override;
};
