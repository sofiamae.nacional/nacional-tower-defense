// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAI.h"
#include "HealthComponent.h"
#include "Waypoint.h"
#include "SpawnPoint.h"
#include "BuffComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AEnemyAI::AEnemyAI()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	Health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
	Health->OnDeath.AddDynamic(this, &AEnemyAI::EnemyDead);
	PawnMovement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("FloatingPawnMovement"));
}

// Called when the game starts or when spawned
void AEnemyAI::BeginPlay()
{
	Super::BeginPlay();

	curWaypoint = 0;
	SpawnDefaultController();
}

// Called every frame
void AEnemyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (curWaypoint >= Waypoints.Num())
	{
		return;
	}

	FRotator rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Waypoints[curWaypoint]->GetActorLocation());
	SetActorRotation(rotation);
	PawnMovement->AddInputVector(GetActorForwardVector());

	if ((GetActorLocation() - Waypoints[curWaypoint]->GetActorLocation()).Size() <= 50)
	{
		curWaypoint++;
	}
	
}

// Called to bind functionality to input
void AEnemyAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyAI::EnemyDead(UHealthComponent* healthComp)
{
	// add coin to player
	Destroy();
}

