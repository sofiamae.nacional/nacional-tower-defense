// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "EnemyAI.h"
#include "TowerData.h"
#include "PlayerActor.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/ArrowComponent.h"
#include "BuffComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);
	
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);
	
	GunTurret = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunTurret"));
	GunTurret->SetupAttachment(RootComponent);
	
	Range = CreateDefaultSubobject<USphereComponent>(TEXT("Range"));
	Range->SetupAttachment(RootComponent);

	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	Arrow->SetupAttachment(RootComponent);
}

void ATower::LookAtEnemy()
{
	if (isShooter)
	{
		FRotator rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), UnitsInRange[0]->GetActorLocation());
		SetActorRotation(rotation);
		//GunTurret->SetWorldRotation(rotation);
	}
}

void ATower::Shoot()
{
	if (canShoot)
	{
		SpawnProjectile();
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Turret shoot"));
		canShoot = false;
		TimerDelay();
	}
}

void ATower::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isSpawned) 
	{
		AddUnitsInRange();
		if (isShooter) // For shooting towers
		{
			if (UnitsInRange.Num() > 0)
			{
				LookAtEnemy();
				Shoot();
			}
		}

		for (int i = 0; i < UnitsInRange.Num(); i++)
		{
			AttachBuff(UnitsInRange[i]);
		}
		
	}
}

void ATower::Initialize()
{
	if (TowerData)
	{
		CurrentMaterial = TowerData->Materials[UpgradeCount];
		StaticMesh->SetMaterial(0, CurrentMaterial);
		GunTurret->SetMaterial(0, CurrentMaterial);

		SphereRadius = TowerData->SphereRadius;
		Damage = TowerData->Damage;
		fireRate = TowerData->fireRate;
		BuffStrength = TowerData->BuffStrength;
		Cost = TowerData->Cost;
		PassiveGold = TowerData->PassiveGold;
		AoERadius = TowerData->AoERadius;
		UpgradeCost = TowerData->UpgradeCost;
		SellCost = Cost / 2;
		
		canUpgrade = true;
		UpgradeCount = 0;
		MaxUpgrade = 2;

		Range->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnBeginOverlap);
		Range->OnComponentEndOverlap.AddDynamic(this, &ATower::OnEndOverlap);
		Range->SetSphereRadius(SphereRadius, true);

		UI_Initialize();
	}

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Initialize"));
}

void ATower::UI_Initialize()
{
	UI_Initialize_Evt();
}

void ATower::AddUnitsInRange()
{
	TArray<AActor*> OverlappingUnits;
	if (targetsEnemy)
	{
		Range->GetOverlappingActors(OverlappingUnits, AEnemyAI::StaticClass());
	}
	else
	{
		Range->GetOverlappingActors(OverlappingUnits, ATower::StaticClass());
	}
	UnitsInRange.Empty();
	for (AActor* Actor : OverlappingUnits)
	{
		UnitsInRange.Add(Actor);
		AttachBuff(Actor);
	}

}

void ATower::OnBeginOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	/*
	if (targetsEnemy)
	{
		if (AEnemyAI* Enemy = Cast<AEnemyAI>(OtherActor))
		{
			UnitsInRange.Add(Enemy);
			//AttachBuff(Enemy);
		}
	}
	else // tower target
	{
		if (ATower* Tower = Cast<ATower>(OtherActor))
		{
			if (Tower->isSpawned)
			{
				UnitsInRange.Add(Tower);
			}
			
			//AttachBuff(Tower);
		}
	}
	*/
	
	
}

void ATower::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (targetsEnemy)
	{
		if (AEnemyAI* Enemy = Cast<AEnemyAI>(OtherActor))
		{
			UnitsInRange.Remove(Enemy);
			RevertEffect(Enemy);
		}
	}
	else
	{
		if (ATower* Tower = Cast<ATower>(OtherActor))
		{
			UnitsInRange.Remove(Tower);
			RevertEffect(Tower);
		}
	}
}

void ATower::UpgradeTower(APlayerActor* Player)
{
	if (UpgradeCount < MaxUpgrade)
	{
		Player->Gold -= UpgradeCost;
		UpgradeTower_Evt();

		UpgradeCount++;
		CurrentMaterial = TowerData->Materials[UpgradeCount];
		StaticMesh->SetMaterial(0, CurrentMaterial);
		GunTurret->SetMaterial(0, CurrentMaterial);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Max Upgrade Reached"));
	}
	
}

void ATower::ReapplyBuff(AActor* Actor)
{
	RevertEffect(Actor);
	AttachBuff(Actor);
}



