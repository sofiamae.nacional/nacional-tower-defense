// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowBuff.h"
#include "EnemyAI.h"
#include "GameFramework/FloatingPawnMovement.h"

void USlowBuff::BuffEffect()
{
	Super::BuffEffect();

	class AEnemyAI* Enemy = Cast<AEnemyAI>(GetOwner());
	Enemy->PawnMovement->MaxSpeed *= BuffStrength;
}

void USlowBuff::RevertBuff()
{
	Super::RevertBuff();

	class AEnemyAI* Enemy = Cast<AEnemyAI>(GetOwner());
	Enemy->PawnMovement->MaxSpeed /= BuffStrength;
}

void USlowBuff::BeginPlay()
{
	Super::BeginPlay();
	//BuffEffect();
}
