// Fill out your copyright notice in the Description page of Project Settings.


#include "PTurretProjectile.h"
#include "EnemyAI.h"
#include "HealthComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

void APTurretProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void APTurretProjectile::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnBeginOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	if (AEnemyAI* Enemy = Cast<AEnemyAI>(OtherActor)) 
	{
		ApplyDamage(Enemy);
		Destroy();
	}
}
