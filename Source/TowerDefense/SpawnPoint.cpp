// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnPoint.h"
#include "EnemyAI.h"
#include "Components/SceneComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASpawnPoint::ASpawnPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));

}

// Called when the game starts or when spawned
void ASpawnPoint::BeginPlay()
{
	Super::BeginPlay();

}

AEnemyAI* ASpawnPoint::spawnEnemies(TSubclassOf<class AEnemyAI> EnemyAI)
{

	FTransform transform = FTransform(FRotator(0), GetActorLocation());
	AEnemyAI* spawnedEnemies = GetWorld()->SpawnActorDeferred<AEnemyAI>(EnemyAI, transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	
	spawnedEnemies->Waypoints = Waypoints;

	return Cast<AEnemyAI>(UGameplayStatics::FinishSpawningActor(spawnedEnemies, transform));
}

// Called every frame
void ASpawnPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

