// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "EnemyAI.generated.h"

UCLASS()
class TOWERDEFENSE_API AEnemyAI : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemyAI();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int32 curWaypoint;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHealthComponent* Health;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UFloatingPawnMovement* PawnMovement;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AActor*> Waypoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Damage;
	
	UFUNCTION()
		void EnemyDead(class UHealthComponent* healthComp);
};
