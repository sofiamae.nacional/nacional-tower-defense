// Fill out your copyright notice in the Description page of Project Settings.


#include "OverloadBuff.h"
#include "Tower.h"

void UOverloadBuff::BuffEffect()
{
	Super::BuffEffect();
	class ATower* Tower = Cast<ATower>(GetOwner());
	Tower->fireRate *= BuffStrength;
}

void UOverloadBuff::RevertBuff()
{
	Super::RevertBuff();
	class ATower* Tower = Cast<ATower>(GetOwner());
	Tower->fireRate /= BuffStrength;
}

void UOverloadBuff::BeginPlay()
{
	Super::BeginPlay();
	//BuffEffect();
}
