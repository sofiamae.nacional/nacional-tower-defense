// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Tower.h"
#include "TowerData.generated.h"


/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UTowerData : public UDataAsset
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString TowerName;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//	FString Upgrades;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Cost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ATower> Tower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float fireRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BuffStrength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SphereRadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AoERadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 PassiveGold;

	// Materials
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Materials")
		TArray<class UMaterial*> Materials;

	// Upgrades
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades") // also used for 
		float FireRateIncrease;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		int32 DamageIncrease;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float SlowIncrease;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float RangeIncrease;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float AoEIncrease;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		int32 GoldIncrease;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		int32 UpgradeCost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		int32 UpgradeCostIncrease;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
	//	int32 SellCost;

	// FOR UI
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FString Stat1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FString Stat2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FString Stat3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FString Value1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FString Value2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FString Value3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI") // Sell cost
		FString Value4;

};
