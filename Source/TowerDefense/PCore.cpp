// Fill out your copyright notice in the Description page of Project Settings.


#include "PCore.h"
#include "HealthComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "EnemyAI.h"

// Sets default values
APCore::APCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);

	Health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	Trigger->SetupAttachment(RootComponent);
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &APCore::OnBeginOverlap);
}

// Called when the game starts or when spawned
void APCore::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APCore::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemyAI* EnemyAI = Cast<AEnemyAI>(OtherActor))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Enemy Hits Core"));
		OnTrigger.Broadcast(EnemyAI);
		EnemyAI->Destroy();
	}
}


