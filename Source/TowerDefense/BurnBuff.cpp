// Fill out your copyright notice in the Description page of Project Settings.


#include "BurnBuff.h"
#include "EnemyAI.h"
#include "HealthComponent.h"
#include "Tower.h"
#include "TimerManager.h"
#include "GameFramework/Actor.h"

void UBurnBuff::BuffEffect()
{
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;
	//FTimerDynamicDelegate Timer

	class AEnemyAI* Enemy = Cast<AEnemyAI>(GetOwner());
	
	BurnDamage();
	//Enemy->StaticMesh->SetMaterial();

	isBurning = true;
	if (duration > 0)
	{
		TimerDelegate.BindLambda([this]
			{
				BuffEffect();
				duration--;
			});

		GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, 2.0f, false);
	}
	else
	{
		DestroyComponent();
	}
}

void UBurnBuff::BurnDamage()
{
	class AEnemyAI* Enemy = Cast<AEnemyAI>(GetOwner());
	Enemy->Health->TakeDamage(BuffDamage);
}

void UBurnBuff::BeginPlay()
{
	Super::BeginPlay();
	duration = 3;
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Burn"));
	BuffEffect();
}
