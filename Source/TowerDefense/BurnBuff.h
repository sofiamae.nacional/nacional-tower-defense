// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffComponent.h"
#include "BurnBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UBurnBuff : public UBuffComponent
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

	int32 duration;
public:
	virtual void BuffEffect() override;
	void BurnDamage();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 BuffDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isBurning;
};
