// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()
public:
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//	float duration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 NumSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AEnemyAI> Enemies; // single enemy type per wave

	//TSubclassOf<ACharacter> Boss;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 EnemyHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 KillReward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 ClearReward;
};
