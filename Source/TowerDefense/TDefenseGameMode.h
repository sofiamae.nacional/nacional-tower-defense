// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDefenseGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATDefenseGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ASpawnPoint*> Spawner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class UWaveData*> Waves;

	FTimerHandle TimerHandle;

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void StartGame();
	UFUNCTION()
		void StartSpawn();
	UFUNCTION()
		void SpawnInterval();
	UFUNCTION()
		void AddGold(class UHealthComponent* Health);
	UFUNCTION()
		void WaveProgression();
	UFUNCTION()
		void EnemyOnHit(AEnemyAI* Enemy); // Wave prog for enemy hit core
	UFUNCTION()
		void WaveCleared();
	UFUNCTION()
		void WavesCompleted();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ABuildManager* BuildManager;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerActor* Player;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 curWave;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 curSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float killCount; // tower and core
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float progressPercent;
};
