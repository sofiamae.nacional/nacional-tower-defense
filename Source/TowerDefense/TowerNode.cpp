// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerNode.h"
#include "Tower.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ATowerNode::ATowerNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);

	//StaticMesh->OnClicked.AddDynamic(this, )
}

// Called when the game starts or when spawned
void ATowerNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATowerNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATowerNode::SpawnOnNode(TSubclassOf<class ATower> Tower)
{
	if (canPlaceTower)
	{
		FTransform transform = FTransform(StaticMesh->GetComponentRotation(), StaticMesh->GetComponentLocation());
		ATower* TowerRef = GetWorld()->SpawnActorDeferred<ATower>(Tower, transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		canPlaceTower = false;
	}
	
}

// Reference : https://www.youtube.com/watch?v=LPGsc8K0zxU&t=46s
