// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeathSignature, UHealthComponent*, Health);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeathNoParam);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable)
		FDeathSignature OnDeath;
	UPROPERTY(BlueprintAssignable)
		FDeathNoParam OnDeathNoParam;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float curHp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float maxHp;
	UFUNCTION()
		void TakeDamage(int32 damage);
	UFUNCTION()
		void Die();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isDead;
};
